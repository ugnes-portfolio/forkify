Desktop version only

# Learning
- Create a file system
- Implement search with an API
- Render data
- Perform calculations
- Create new recipes
- Bookmark recipes to localStorage

# How to use
- Type the meal name into the search bar
- Browse the recipes
- You can choose how many servings you want, and the amount of ingredients will be calculated
- You can bookmark a recipe; it will be found in bookmarks (saved in localStorage)
- You can create your recipe by clicking "Add recipe." These recipes will be displayed only to you
