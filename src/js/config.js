export const API_URL = 'https://forkify-api.herokuapp.com/api/v2/recipes/';
export const TIMEOUT_SEC = 10;
export const RES_PER_PAGE = 10;
export const KEY = 'a4556e29-cbee-49f6-b4b2-5fef894f34e4';
export const MODAL_CLOSE_SEC = 2.5;
